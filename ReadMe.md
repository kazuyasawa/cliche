
```
docker build -t django-blog-image:latest /Users/asawakazuya/workspace/cliche/sample
docker run --rm -it -p 8000:8000 -v /Users/asawakazuya/workspace/cliche/sample:/root/mysite --name sampleblog django-blog-image:latest /bin/bash
```

```
pip3 install -r requirements.txt
pip3 install social-auth-app-django
python3 manage.py runserver 0.0.0.0:8000
```

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django',
        'USER': 'root',
    'PASSWORD': 'mariadb',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
                'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
                },
    }
}

```
