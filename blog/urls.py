from django.urls import path
from . import views
urlpatterns = [
    path('',views.TopView.as_view(),name='topView'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new', views.PostView.as_view(), name='post_new'),
    #path('answer/',views.AnswerView.as_view(),name='answerView'),
    ]
