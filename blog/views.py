from django.shortcuts import render,redirect, get_object_or_404
from django.urls import reverse
from django.views import View
from django.utils import timezone
from .models import Post
from .forms import PostForm
# Create your views here.

class TopView(View):
    def get(self,request,*args,**kwargs):
        posts=Post.objects.filter(created_date__lte=timezone.now()).order_by('created_date')
        return render(request,'blog/top.html',{'posts':posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

class PostView(View):
    def get(self,request,*args,**kwargs):
        form = PostForm()
        return render(request,'blog/edit.html',{'form':form})
    def post(self,request,*args,**kwargs):
        form = PostForm(request.POST)
        post = form.save(commit=False)
        post.author = request.user
        post.created_date = timezone.now()
        post.save()
        return redirect('post_detail', pk=post.pk)

'''
class TopView(View):
    def get(self,request,*args,**kwargs):
        #TBD
        return render(request,'blog/top.html',{'form':form})
    def post(self,request,*args,**kwargs):
        form = UserAnswerForm(request.POST)
        return render(request,'quiz/answer.html',{'correctAnswer':correctAnswer,'answerInput':answerInput})
'''
